package com.stc.tracnghiembe.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 4/5/23
 * Time      : 7:32 AM
 * Filename  : LoaiCauHoi
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "loai-cau-hoi")
public class LoaiCauHoi extends BaseEntity {
    @Id
    private String id;

    private String maLoai;

    private String tenLoai;

    private boolean trangThai = true;
}
