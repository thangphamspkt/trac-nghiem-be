package com.stc.tracnghiembe.repositories;

import com.stc.tracnghiembe.entities.LoaiCauHoi;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 4/5/23
 * Time      : 7:33 AM
 * Filename  : LoaiCauHoiRepository
 */
public interface LoaiCauHoiRepository extends MongoRepository<LoaiCauHoi, String> {

    Optional<LoaiCauHoi> findByMaLoaiIgnoreCase(String maLoai);

    @Query(value = "{'maLoai': ?0}", exists = true)
    boolean kiemTraMaLoai(String maLoai);

}
