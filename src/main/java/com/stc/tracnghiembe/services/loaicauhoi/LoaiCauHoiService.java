package com.stc.tracnghiembe.services.loaicauhoi;

import com.stc.tracnghiembe.dtos.loaicauhoi.LoaiCauHoiDto;
import com.stc.tracnghiembe.entities.LoaiCauHoi;

import java.security.Principal;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 4/5/23
 * Time      : 8:03 AM
 * Filename  : LoaiCauHoiService
 */
public interface LoaiCauHoiService {

    LoaiCauHoi getLoaiCauHoi(String id);

    LoaiCauHoi create(LoaiCauHoiDto dto, Principal principal);

    LoaiCauHoi update(String id, LoaiCauHoiDto dto, Principal principal);
}
