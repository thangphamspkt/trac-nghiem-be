package com.stc.tracnghiembe.services.loaicauhoi;

import com.stc.tracnghiembe.dtos.loaicauhoi.LoaiCauHoiDto;
import com.stc.tracnghiembe.entities.LoaiCauHoi;
import com.stc.tracnghiembe.exceptions.InvalidException;
import com.stc.tracnghiembe.exceptions.NotFoundException;
import com.stc.tracnghiembe.repositories.LoaiCauHoiRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.security.Principal;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 4/5/23
 * Time      : 8:03 AM
 * Filename  : LoaiCauHoiServiceImpl
 */
@Slf4j
@Service
public class LoaiCauHoiServiceImpl implements LoaiCauHoiService {
    private final LoaiCauHoiRepository loaiCauHoiRepository;

    public LoaiCauHoiServiceImpl(LoaiCauHoiRepository loaiCauHoiRepository) {
        this.loaiCauHoiRepository = loaiCauHoiRepository;
    }


    @Override
    public LoaiCauHoi getLoaiCauHoi(String id) {
        return loaiCauHoiRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String
                        .format("Loại câu hỏi có id %s không tồn tại", id)));
    }

    @Override
    public LoaiCauHoi create(LoaiCauHoiDto dto, Principal principal) {
        if (ObjectUtils.isEmpty(dto.getMaLoai())) {
            throw new InvalidException("Mã loại câu hỏi không được bỏ trống");
        }
        if (ObjectUtils.isEmpty(dto.getTenLoai())) {
            throw new InvalidException("Tên loại câu hỏi không được bỏ trống");
        }
        if (loaiCauHoiRepository.kiemTraMaLoai(dto.getMaLoai().trim().toUpperCase())) {
            throw new InvalidException(String.format("Loại câu hỏi có mã %s đã tồn tại",
                    dto.getMaLoai()));
        }
        LoaiCauHoi loaiCauHoi = new LoaiCauHoi();
        loaiCauHoi.setMaLoai(dto.getMaLoai().trim().toUpperCase());
        loaiCauHoi.setTenLoai(dto.getTenLoai().trim());
        loaiCauHoiRepository.save(loaiCauHoi);
        return loaiCauHoi;
    }

    @Override
    public LoaiCauHoi update(String id, LoaiCauHoiDto dto, Principal principal) {
        LoaiCauHoi loaiCauHoi = getLoaiCauHoi(id);
        if (ObjectUtils.isEmpty(dto.getMaLoai())) {
            throw new InvalidException("Mã loại câu hỏi không được bỏ trống");
        }
        if (ObjectUtils.isEmpty(dto.getTenLoai())) {
            throw new InvalidException("Tên loại câu hỏi không được bỏ trống");
        }
        if (!loaiCauHoi.getMaLoai().equalsIgnoreCase(dto.getMaLoai().trim())
                && loaiCauHoiRepository.kiemTraMaLoai(dto.getMaLoai().trim().toUpperCase())) {
            throw new InvalidException(String.format("Loại câu hỏi có mã %s đã tồn tại",
                    dto.getMaLoai()));
        }
        loaiCauHoi.setMaLoai(dto.getMaLoai().trim().toUpperCase());
        loaiCauHoi.setTenLoai(dto.getTenLoai().trim());
        loaiCauHoiRepository.save(loaiCauHoi);
        return loaiCauHoi;
    }
}
