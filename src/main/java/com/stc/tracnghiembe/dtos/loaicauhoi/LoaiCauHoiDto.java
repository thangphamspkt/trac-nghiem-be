package com.stc.tracnghiembe.dtos.loaicauhoi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 4/5/23
 * Time      : 8:02 AM
 * Filename  : LoaiCauHoiDto
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoaiCauHoiDto {
    private String maLoai;

    private String tenLoai;
}
