package com.stc.tracnghiembe.controllers;

import com.stc.tracnghiembe.dtos.loaicauhoi.LoaiCauHoiDto;
import com.stc.tracnghiembe.entities.LoaiCauHoi;
import com.stc.tracnghiembe.services.loaicauhoi.LoaiCauHoiService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

/**
 * Created by: IntelliJ IDEA
 * User      : thangpx
 * Date      : 4/5/23
 * Time      : 8:10 AM
 * Filename  : LoaiCauHoiController
 */
@RestController
@RequestMapping("/rest/loai-cau-hoi")
public class LoaiCauHoiController {
    private final LoaiCauHoiService loaiCauHoiService;

    public LoaiCauHoiController(LoaiCauHoiService loaiCauHoiService) {
        this.loaiCauHoiService = loaiCauHoiService;
    }

    /***
     * @author: thangpx
     * @createDate: 05/04/2023
     * @param dto: mã loại, và tên loại
     * @param principal: token
     * @return: LoaiCauHoi
     */
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Create loại câu hỏi")
    @PostMapping
    public ResponseEntity<LoaiCauHoi> create(@Valid @RequestBody LoaiCauHoiDto dto,
                                             Principal principal) {
        return new ResponseEntity<>(loaiCauHoiService.create(dto, principal), HttpStatus.OK);
    }

    /***
     * @author: thangpx
     * @createDate: 05/04/2023
     * @param id: ID loại câu hỏi cần update
     * @param dto: thông tin loại câu hỏi cần update
     * @param principal: current user's token
     * @return: LoaiCauHoi
     */
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Update loại câu hỏi by id, cho cập nhật mã loại")
    @PutMapping("/{id}")
    public ResponseEntity<LoaiCauHoi> update(@PathVariable String id, @Valid @RequestBody LoaiCauHoiDto dto,
                                             Principal principal) {
        return new ResponseEntity<>(loaiCauHoiService.update(id, dto, principal), HttpStatus.OK);
    }
}
